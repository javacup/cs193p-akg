//
//  CalculatorViewController.h
//  Calculator
//
//  Created by Kalyan Dasika on 6/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalculatorViewController : UIViewController
- (IBAction)digitPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *display;
- (IBAction)operationPressed:(id)sender;
- (IBAction)SEnterPressed;
@property (weak, nonatomic) IBOutlet UILabel *debugWindow;
- (IBAction)decimalPressed:(id)sender;
- (IBAction)clearPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *stackWindow;
@property (weak, nonatomic) IBOutlet UILabel *numberSentenceLabel;
- (IBAction)backButtonPressed:(id)sender;
- (IBAction)changeSign:(id)sender;

@end
