//
//  CalculatorBrain.m
//  Calculator
//
//  Created by Kalyan Dasika on 6/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//  A sample comment.
//

#import "CalculatorBrain.h"

@interface CalculatorBrain()
@property (nonatomic,strong) NSMutableArray *operandStack;
@end

@implementation CalculatorBrain

@synthesize operandStack=_operandStack;

-(NSMutableArray *) operandStack{
    return _operandStack;
}
-(void) setOperandStack:(NSMutableArray *)anArray{
    _operandStack = anArray;
}

-(void) pushOperand:(double) operand{
    NSNumber *operandObject = [NSNumber numberWithDouble:operand];
    NSLog(@"pushOperand : %f",operand);
    if(!_operandStack){
        _operandStack = [[NSMutableArray alloc]init];
    }
    [_operandStack addObject:operandObject];
}
-(double) popOperand{
    NSNumber *operandObject = [_operandStack lastObject];
    if(operandObject){
        [_operandStack removeLastObject];
    }
    NSLog(@"operand : %f",[operandObject doubleValue]);
    return [operandObject doubleValue];
}
-(double) performOperation:(NSString *)operation{
    double result = 0;
    NSLog(@"Operation: %@",operation);
    
    if([operation isEqualToString:@"+"]){
        result = [self popOperand] + [self popOperand];
    }else if([@"*" isEqualToString:operation]){
        result = [self popOperand] * [self popOperand];        
    }else if([@"/" isEqualToString:operation]){
        double divisor = [self popOperand];
        if(divisor) result = [self popOperand] / divisor;        
    }else if([@"-" isEqualToString:operation]){
        double subtrahend = [self popOperand];
        result = [self popOperand] - subtrahend;        
    }else if([@"sin" isEqualToString:operation]){
        result = sin([self popOperand] * (M_PI/180));;        
    }else if([@"cos" isEqualToString:operation]){
        result = cos([self popOperand] * (M_PI/180));;        
    }else if([@"sqrt" isEqualToString:operation]){
        result = sqrt([self popOperand]);;        
    }
    [self pushOperand:result];
    return result;
}
-(void)clearOperandStack{
    NSLog(@"Clear Operand Stack");
    [self.operandStack removeAllObjects];
}

@end
