//
//  CalculatorViewController.m
//  Calculator
//
//  Created by Kalyan Dasika on 6/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CalculatorViewController.h"
#import "CalculatorBrain.h"

@interface CalculatorViewController ()
@property (nonatomic) BOOL userIsInTheMiddleOfEnteringANumber;
@property (nonatomic,strong) CalculatorBrain *brain;
@property (nonatomic,weak) UIButton *back;
@property (nonatomic) BOOL currentSignNegative;
-(void)pushDebugWindow:(NSString *)message;
-(void) appendToNumberSentenceLabel:(NSString *)term;
@end

@implementation CalculatorViewController
@synthesize stackWindow;
@synthesize numberSentenceLabel;
@synthesize debugWindow=_debugWindow;
@synthesize display;
@synthesize userIsInTheMiddleOfEnteringANumber;
@synthesize currentSignNegative;
@synthesize brain = _brain;
@synthesize back;

-(CalculatorBrain *) brain{
    if(!_brain) _brain = [[CalculatorBrain alloc]init];
    return _brain;
}

- (IBAction)digitPressed:(UIButton *)sender {
    NSString *digit = [sender currentTitle];
    NSString *debugMsg = [NSString stringWithFormat:@"Digit pressed: %@",digit];
    [self pushDebugWindow:debugMsg];

    if([digit isEqualToString:@"π"]){
        if(![self.display.text isEqualToString:@"0"]){
            [self SEnterPressed];        
        }
        self.userIsInTheMiddleOfEnteringANumber = NO; 
    }
    
    if(self.userIsInTheMiddleOfEnteringANumber){
        self.display.text = [self.display.text stringByAppendingString:digit];
    }else{
        if(self.currentSignNegative){
            digit = [@"-" stringByAppendingString:digit];
            self.currentSignNegative = NO;
        }
        self.display.text = digit;
        self.userIsInTheMiddleOfEnteringANumber = YES; 
    }
    if([digit isEqualToString:@"π"]){
        [self SEnterPressed];        
        self.userIsInTheMiddleOfEnteringANumber = NO; 
    }
    back.enabled = TRUE;
}
- (IBAction)operationPressed:(UIButton *)sender {
    [self pushDebugWindow: [@"Operation pressed: " stringByAppendingString:sender.currentTitle]];
    if(self.userIsInTheMiddleOfEnteringANumber){
        [self SEnterPressed];
    }
    NSString *operation = [sender currentTitle];
    double result = [self.brain performOperation:operation];
    self.display.text = [NSString stringWithFormat:@"%g",result];
    [self appendToNumberSentenceLabel:operation];
    [self appendToNumberSentenceLabel:@" = "];
    [self appendToNumberSentenceLabel:self.display.text];
    back.enabled = NO;
}

- (IBAction)SEnterPressed {
    [self pushDebugWindow:@"Enter pressed.."];
    if([self.display.text isEqualToString:@"π"]){
        [self.brain pushOperand:M_PI];        
    }else {
        [self.brain pushOperand:[self.display.text doubleValue]];
    }
    self.userIsInTheMiddleOfEnteringANumber = NO;
    [self appendToNumberSentenceLabel:self.display.text];
    back.enabled = NO;
}

- (void)pushDebugWindow:(NSString *)message{
    NSString *finalMsg = [self.debugWindow.text stringByAppendingString:[@"\n" stringByAppendingString:message]];
    self.debugWindow.text = finalMsg;
    
}

- (void)viewDidUnload {
    [self setDebugWindow:nil];
    [self setStackWindow:nil];
    [self setNumberSentenceLabel:nil];
    [super viewDidUnload];
}
- (IBAction)decimalPressed:(id)sender {
    NSString *digit = [sender currentTitle];
    self.display.text = [self.display.text stringByAppendingString:digit];
    self.userIsInTheMiddleOfEnteringANumber = YES; 
}

- (IBAction)clearPressed:(id)sender {
   [self pushDebugWindow:@"Clear pressed.."];
    self.display.text = @"0";
    self.debugWindow.text=@"";
    self.numberSentenceLabel.text=@"";
    self.userIsInTheMiddleOfEnteringANumber = NO;    
    [self.brain clearOperandStack];
    back.enabled = NO;
}
-(void) appendToNumberSentenceLabel:(NSString *)term{
    if(term)
        self.numberSentenceLabel.text = [self.numberSentenceLabel.text stringByAppendingString:term];
    if(![term isEqualToString:@"."])
        self.numberSentenceLabel.text = [self.numberSentenceLabel.text stringByAppendingString:@" "];
}

- (IBAction)backButtonPressed:(id)sender {
    back = sender;
    NSString *currentText = self.display.text;
    NSInteger indexToLastCharacter = currentText.length;
    indexToLastCharacter = indexToLastCharacter-1;
    if(indexToLastCharacter>0)
        currentText = [currentText substringToIndex:indexToLastCharacter];
    NSLog(@"Trimmed to %@",currentText);
    self.display.text=currentText;    
}

- (IBAction)changeSign:(id)sender {
    double currentNumber = [self.display.text doubleValue];
    if(self.userIsInTheMiddleOfEnteringANumber){
        currentNumber = currentNumber * (-1);
        NSString *currentNumberInString = [NSString stringWithFormat:@"%g",currentNumber];
        self.display.text = currentNumberInString;        
    }else{
        if(self.currentSignNegative == YES){
            self.currentSignNegative = NO;
        }else{
            self.currentSignNegative = YES;
        }
    }
}
@end
